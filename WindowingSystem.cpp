/*
 *      We will use this template for all OpenGL examples in ICP3036
 *
 *      The main GLUT routine is set up here
 */
#include "WindowingSystem.h"		// Header File For WindowingSystem.cpp

// Main routine: defines window properties, creates window,
// registers callback routines and begins processing.
int main(int argc, char **argv) 
{  
   // Initialize GLUT.
   glutInit(&argc, argv);
 
   // Set display mode with an RGB colour buffer, double buffering and a depth buffer..
   glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH ); 
   
   // Set OpenGL window size
   glutInitWindowSize(500, 500);

   // Set position of OpenGL window upper-left corner
   glutInitWindowPosition(100, 100); 
   
   // Create OpenGL window with title
   glutCreateWindow("Cube Runner Game");
   
   // Initialize
   setup(); 
   
   // Register display routine
   glutDisplayFunc(drawScene); 
   
   // Register reshape routine
   glutReshapeFunc(resize);  

   // Register keyboard routine
   glutKeyboardFunc(keyInput);
  
  // Register keyboard up routine
   glutKeyboardUpFunc(keyUp);
   
   // The special keyboard callback is triggered when keyboard function or directional keys are pressed.
   glutSpecialFunc( arrow_keys );
   
   // Register the mouse callback
   glutMouseFunc( mouseButton );
   
   // This is needed for continuous animation when window system events are not being received. 
  glutTimerFunc(TIMERDELAY, updateScene, 0);
  
   // Begin processing
   glutMainLoop(); 

   return 0;  
}
