/*
 *      We will use this template for all OpenGL examples in ICP3036
 *
 *      This file must contain your code for:
 *           void drawScene(GLvoid);
 *           void resize(int w, int h);
 *           void setup(void);
 *           void keyInput(unsigned char key, int x, int y);
 */
 
#include "WindowingSystem.h"                // Header File for WindowingSystem.cpp
#include <math.h>
#include <vector>
#include <iostream>
#include <stdio.h>
#include "windows.h"
#include "mmsystem.h"






using namespace std;

#define MAX_NO_TEXTURES 2

#define CUBE_TEXTURE 0
#define FCUBE_TEXTURE 1

GLuint texture_id[MAX_NO_TEXTURES];

float xrot;
float yrot;
float xspeed;			// X Rotation Speed
float yspeed;
float xspeed1;			// X Rotation Speed
float yspeed1;			// Y Rotation Speed
float z=-4.0f;		    // Depth Into The Screen

bool quit;
int score;
int progress;
float speed;

GLfloat texture[10];




vector <vector <float> > cubeArray(400,vector<float>(4)); 

int hit = 0;
GLuint cubes;
GLuint runner;

GLfloat LightAmbient[]=		{ 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat LightDiffuse[]=		{ 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightPosition[]=	{ 0.0f, 0.0f, 2.0f, 1.0f };

GLuint	filter;				// Which Filter To Use
bool	light=TRUE;			// Lighting ON/OFF
bool	lp;					// L Pressed?
bool    lbut=FALSE;
bool    rbut=FALSE;

void Makematerial( float r,float g,float b,float a,float s )  
{   
float color[4];  
float white[4]; 
GLfloat surfshine[1] ; 
surfshine[0]=s*128.0 ; 
color[0]=r;color[1]=g;color[2]=b;color[3]=a;  
white[0]=0.5;white[1]=0.5;white[2]=0.5;white[3]=1.0;   
glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT,color);  
glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE,color);  
glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR,white); 
glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS,surfshine); 
}  





void BuildList(void)
{
     GLUquadricObj *qobj;
     cubes = glGenLists(1);
     glNewList(cubes,GL_COMPILE);
     //code for cube 
      glBegin ( GL_QUADS );
   // Front Face
		glNormal3f( 0.0f, 0.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
		// Back Face
		glNormal3f( 0.0f, 0.0f,-1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
		// Top Face
		glNormal3f( 0.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
		// Bottom Face
		glNormal3f( 0.0f,-1.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
		// Right face
		glNormal3f( 1.0f, 0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
		// Left Face
		glNormal3f(-1.0f, 0.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
	glEnd();
	
	
	glEndList();
    
     
     
    //spaceship list
    runner = glGenLists(1);
     glNewList(runner,GL_COMPILE);
    float eye[3]; 
int order; 
//enum knottype knots; 
int silend;  
float sil[70][2]; 




qobj = gluNewQuadric(); 
glEnable(GL_NORMALIZE); 
gluQuadricNormals(qobj,GLU_SMOOTH); 
gluQuadricDrawStyle(qobj,GLU_FILL); 

////////////////// cone ///////////////////////////////// 
Makematerial(0.17f,0.80f,0.67f,1.00f, 0.8f);
glPushMatrix(); 
glTranslatef( 0.4f, 0.8f, 0.9f); 
glRotatef( 0,0,1,0); 
glRotatef( 0,1,0,0); 
glRotatef( 0,0,0,1); 
glScalef( 0.24f, 0.01f, 0.24f); 
gluCylinder(qobj,0.0,5.0,12,36,2); 
glPopMatrix(); 
////////////////// cone ///////////////////////////////// 
Makematerial(0.17f,0.80f,0.67f,1.00f, 0.8f);
glPushMatrix(); 
glTranslatef( 0.4f, 0.8f, 0.9f); 
glRotatef( -15,0,1,0); 
glRotatef( -5,1,0,0); 
glRotatef( 30,0,0,1); 
glScalef( 0.12f, 0.01f, 0.26f); 
gluCylinder(qobj,0.0,5.0,12,36,2); 
glPopMatrix(); 
////////////////// cone ///////////////////////////////// 
Makematerial(0.19f,0.80f,0.67f,1.00f, 0.8f);
glPushMatrix(); 
glTranslatef( 0.4f, 0.8f, 0.9f); 
glRotatef( 14,0,1,0); 
glRotatef( -5,1,0,0); 
glRotatef( -30,0,0,1); 
glScalef( 0.12f, 0.01f, 0.26f); 
gluCylinder(qobj,0.0,5.0,12,36,2); 
glPopMatrix(); 
////////////////// cone ///////////////////////////////// 
Makematerial(0.17f,0.80f,0.67f,1.00f, 0.8f);
glPushMatrix(); 
glTranslatef( 0.4f, 0.8f, 0.9f); 
glRotatef( -1,0,1,0); 
glRotatef( -10,1,0,0); 
glRotatef( 0,0,0,1); 
glScalef( 0.06f, 0.01f, 0.26f); 
gluCylinder(qobj,0.0,5.0,12,36,2); 
glPopMatrix(); 

glEndList();
       
}

bool detectcollision(float x1, float y1, float z1,float radius1, float x2, float y2, float z2,float radius2)
{
    float xd, yd, zd, Distance, fin;
    
          if(x1 > x2){
          xd = x1-x2;      
          }else
          {
        xd = x2-x1;
        }
          if(y1 > y2){
          yd = y1-y2;      
          }else
          {
        yd = y2-y1;
        }
        if(z1 > z2){
          zd = z1-z2;      
          }else
          {
        zd = z2-z1;
        }
        
      
       
        
        fin = xd*xd + yd*yd + zd*zd;
        Distance = sqrt((int) fin);

     
    if( radius1 + radius2 >= Distance)
        return true; //collision
    return false;    //no collision    
}

void spawn(int i)
{
              int randx = (rand()%(((int)yspeed-75)-((int)yspeed+75)))+((int)yspeed-75);
              cubeArray[i][0] = (float)randx;
             //y  
             int randy = (rand()%(((int)xspeed-75)-((int)xspeed+75)))+((int)xspeed-75);
             cubeArray[i][1] = (float)randy;
             //z  
              cubeArray[i][2] = -20.0f;
             //texture 
            if((rand() % (0-10)) >= 9)
             {
              cubeArray[i][3] = 0;
             
             }
             else 
             {
              cubeArray[i][3] = 1;
             
              }
             
             
            
 
 
 }


/* This is where you put all your OpenGL drawing commands */
void drawScene(void)									// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
	glLoadIdentity();
     glMatrixMode(GL_MODELVIEW);							// Select the Modelview Matrix
	glLoadIdentity();
    									// Reset the Modelview Matrix
	gluLookAt(yspeed,xspeed+4, 100, //eye
		      yspeed,xspeed -2,0, //at
			  0.0f,1.0f,0.0f);	//up								// Reset The Current Modelview Matrix
	
 	glPushMatrix();
	if(lbut)  z+=0.02f;     // Zoom in while left button pressed
	else if(rbut) z-=0.02f; // Zoom out while right button pressed
  


  //move function
  for(int i = 0; i < cubeArray.size();i++){              
     
              if(cubeArray[i][3] < 1.0f)          
            {
            glBindTexture ( GL_TEXTURE_2D, texture_id[0] );
    
                  cubeArray[i][2] += speed*2;
            }
            else 
            {
                if(score < 1500)
                {
                 glBindTexture ( GL_TEXTURE_2D, texture_id[3] );       
                  speed = 0.5f;
                }
                else if(score > 1500 && score < 3000)
                {
                glBindTexture ( GL_TEXTURE_2D, texture_id[2] );       
                 speed = 1.0f;
                }
                else if(score > 3000 && score < 4500)
                {
                glBindTexture ( GL_TEXTURE_2D, texture_id[3] );       
                 speed = 1.5f;
                }
                else if(score > 4500 && score < 6000)
                {
                glBindTexture ( GL_TEXTURE_2D, texture_id[4]);       
                 speed = 2.0f;
                }
                 else if(score > 6000 && score < 7500)
                {
                glBindTexture ( GL_TEXTURE_2D, texture_id[5] );       
                 speed = 2.5f;
                }
                 else if(score > 7500)
                {
                glBindTexture ( GL_TEXTURE_2D, texture_id[6] );       
                 speed = 3.0f;
                }
                 cubeArray[i][2] += speed; 
                 
            }    
            
            
             if(cubeArray[i][2] > 95.0f)
             {                  
              spawn(i); //move cube back                  
             }
           
        
              glTranslatef(cubeArray[i][0] , cubeArray[i][1], cubeArray[i][2]);       
             
    glCallList(cubes);
    glTranslatef(-cubeArray[i][0] , -cubeArray[i][1], -cubeArray[i][2]);
}


 glPopMatrix(); 
 


       glPushMatrix();
       
       glBindTexture(GL_TEXTURE_2D,texture_id[2]);
       glTranslatef(yspeed,xspeed-2,90);
       glCallList(runner);
       glTranslatef(-yspeed,-xspeed-2,-90);
       
        glPopMatrix();
          
 
   for(int i = 0; i < cubeArray.size();i++){ 
    if (detectcollision(yspeed,xspeed,93,0.5f,cubeArray[i][0],cubeArray[i][1],cubeArray[i][2],1.0f))
    {
       quit++;
                                                        
    }
    else if (detectcollision(yspeed+1,xspeed,94,0,cubeArray[i][0],cubeArray[i][1],cubeArray[i][2],1.0f))
    {
        
       quit++;
                                                            
    }
     else if (detectcollision(yspeed-1,xspeed,94,0,cubeArray[i][0],cubeArray[i][1],cubeArray[i][2],1.0f))
    {
       quit++;
                                                         
    }
}
    
    if(quit > 0)
    {
             PlaySound("Data/collision_blow_up_sound.wav", NULL,NULL);
          PlaySound("Data/game_over_after_collision.wav", NULL,NULL);
          
           exit(0);  
    }
    
     score += 1;
    glutSwapBuffers();      //swaps the front and back buffers
}




void updateScene(int value)
{
glutTimerFunc( TIMERDELAY, updateScene, 0 ); // Reset timer tick
glutPostRedisplay(); // activates drawScene callback
}

/* Initialisation routine - acts like your typical constructor in a Java program. */
void setup(void)										// All Setup For OpenGL Goes Here
{
     
    
      PlaySound("Data/game_music.wav", NULL,SND_LOOP|SND_ASYNC);
     speed = 1.0f;
        for(int i = 0;i<cubeArray.size();i++){   
             //x 
            int randx = (rand()%(((int)yspeed-75)-((int)yspeed+75)))+((int)yspeed-75);
              cubeArray[i][0] = (float)randx;
             //y  
             int randy = (rand()%(((int)xspeed-75)-((int)xspeed+75)))+((int)xspeed-75);
             cubeArray[i][1] = (float)randy;
             //z  
             int randz = (rand()%(-50-20))+-50;
              cubeArray[i][2] = randz;
             //texture 
             if((rand() % (0-10)) >= 9)
             {
              cubeArray[i][3] = 0;
             
             }
             else 
             {
              cubeArray[i][3] = 1;
             
              }
             
             }
         
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// Type of depth testing to do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
    BuildList();
	glEnable ( GL_TEXTURE_2D );
    glGenTextures (5, texture_id);

    // Load in the BMP image
    BMPClass bmp;
    BMPLoad( "data/bangor.bmp", bmp );
    
    // This texture uses a nearest neighbour filter
    glBindTexture ( GL_TEXTURE_2D, texture_id [0] );
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    
    BMPLoad( "data/funk.bmp", bmp );
    // This texture will be better quality as it uses Linear interpolation
    glBindTexture ( GL_TEXTURE_2D, texture_id [1] );
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
  
  BMPLoad( "data/siliconCircuit.bmp", bmp );
    // This texture will be better quality as it uses Linear interpolation
    glBindTexture ( GL_TEXTURE_2D, texture_id [2] );
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
  
  BMPLoad( "data/pineapple.bmp", bmp );
    // This texture will be better quality as it uses Linear interpolation
    glBindTexture ( GL_TEXTURE_2D, texture_id [3] );
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
    
    BMPLoad( "data/wierd.bmp", bmp );
    // This texture will be better quality as it uses Linear interpolation
    glBindTexture ( GL_TEXTURE_2D, texture_id [4] );
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
  
    BMPLoad( "data/sun.bmp", bmp );
    // This texture will be better quality as it uses Linear interpolation
    glBindTexture ( GL_TEXTURE_2D, texture_id [5] );
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
   
     BMPLoad( "data/coregated.bmp", bmp );
    // This texture will be better quality as it uses Linear interpolation
    glBindTexture ( GL_TEXTURE_2D, texture_id [6] );
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,3,bmp.width,bmp.height,0,GL_RGB,GL_UNSIGNED_BYTE,bmp.bytes);
  
  
  
    glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);		// Setup The Ambient Light
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);		// Setup The Diffuse Light
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition);	// Position The Light
	glEnable(GL_LIGHT1);								// Enable Light One
}

/* Tells the program how to resize your OpenGL Window */
void resize(int width, int height)
{
       
	if (height==0)										// Prevent a divide by zero by
	{										            // making height equal one
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);		// Set viewport size to be entire OpenGL window

	glMatrixMode(GL_PROJECTION);						// Select the Projection Matrix
	glLoadIdentity();									// Reset the Projection Matrix

	// Calculate The Aspect Ratio Of The Window
	gluPerspective(80.0f,(GLfloat)width/(GLfloat)height,1.0f,200.0f);

	glMatrixMode(GL_MODELVIEW);							// Select the Modelview Matrix
	glLoadIdentity();									// Reset the Modelview Matrix
	
}

/* Keyboard input processing routine */
void keyInput(unsigned char key, int x, int y)
{
   switch(key) 
   {
	  // Press escape to exit.
      case 27:
           exit(0);
           break;
      case 'l': // turn light on and off
	       lp=TRUE;
           light=!light;
		   if (!light) glDisable(GL_LIGHTING);
		   else glEnable(GL_LIGHTING);
       default:
           break;
   }
}

void keyUp(unsigned char key, int x, int y)
{
   switch(key) 
   {
      case 'f': // toggle between the two texture filters loaded
           filter+=1;
		   if (filter>1) filter=0;
    	   break;
    	  
       default:
           break;
   }
}

void arrow_keys ( int a_keys, int x, int y )  // Create Special Function (required for arrow keys)
{
  switch ( a_keys ) {
    case GLUT_KEY_UP:     // When Up Arrow is Pressed...
      xspeed+=1.0f;
    break;  
    case GLUT_KEY_DOWN:   // When Down Arrow is Pressed...
      xspeed-=1.0f;
      break;
    case GLUT_KEY_RIGHT:  // When Right Arrow is Pressed...
    	yspeed+=1.0f;
      break;
    case GLUT_KEY_LEFT:  // When Left Arrow is Pressed...
    	yspeed-=1.0f;
    	break;
    case GLUT_KEY_F1:    // When F1 key is Pressed...
    	glutFullScreen ();
      break;
    case GLUT_KEY_F2:    // When F2 key is Pressed...
    	glutReshapeWindow ( 500, 500 );
      break;
 
    default:
      break;
  }
}

void mouseButton(int button, int state, int x, int y)
{
     if (button == GLUT_LEFT_BUTTON) {
        if( state == GLUT_DOWN) lbut = TRUE; // pressed
        else lbut = FALSE;                   // released
     }
     else if (button == GLUT_RIGHT_BUTTON) {
        if( state == GLUT_DOWN) rbut = TRUE; // pressed
        else rbut = FALSE;                   // released
     }
}
