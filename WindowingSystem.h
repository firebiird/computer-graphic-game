#ifndef WINDOWINGSYSTEM_H
#define WINDOWINGSYSTEM_H

#include <gl\glut.h>		// Header file for the glut library.
                            // glut.h calls gl.h and glu.h

#include <stdio.h>          // Header File For Standard Input / Output
#include <stdarg.h>			// Header File For Variable Argument Routines
#include <iostream>

#include "BMPLoader.h"

#define TIMERDELAY 10


// These are the functions that you will use to create and interact with your GL scene:
void drawScene(GLvoid);
void updateScene(int value);
void resize(int w, int h);
void setup(void);
void keyInput(unsigned char key, int x, int y);
void keyUp(unsigned char key, int x, int y);
void arrow_keys ( int a_keys, int x, int y ) ;
void mouseButton(int button, int state, int x, int y);
#endif
